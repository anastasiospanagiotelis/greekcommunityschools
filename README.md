This repository contains all the files involved in building [this app](https://anastasiospanagiotelis.shinyapps.io/GreekMapAppSchools/) on the distribution of Greek people and Greek Language schools in in Melbourne. 

There are three directories:

1. Preliminary contains everything needed to process the data before using the app.  Just look at the file Preprocess.pdf.
2. GreekMapAppSchools contains the actual R code for building the shiny app, in the file app.R
3. Presentation contains the files used for the presentation.  The presentation itself is GreekSchools.html.