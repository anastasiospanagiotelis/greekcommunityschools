Building an Interactive Choropleth using Shiny
========================================================
author: Anastasios Panagiotelis
date: 2nd June, 2017
width: 1366
height: 768
incremental: true

The Project
========================================================

- The Greek Orthodox Community of Melbourne and Victoria (GOCMV) operate a number of Greek Language schools in the Greater Melbourne Area.
- A key factor in retaining students is proximity to the school.
- New schools should be set up in areas with highest demand and where there are few existing schools.
- Both the population of Greeks and the locations of existing schools can be easily visualised on a map.
- The aim was to develop and fine tune a visualisation based on 2011 census data, and then update this with 2016 data.

Who is Greek?
========================================================

- Multiple census questions can be used to measure the number of Greek people:  
    - Ancestry,
    - Language other than English Spoken at Home,
    - Place of Birth,
    - Religion.
- Also many Cypriots speak Greek and would be interested in language classes.
- No single census question is a perfect measure.
- The user of the visualisation should be able to select *census question*.

Which Greeks need schools?
========================================================

- Clearly there is a greater interest in the distribution of younger age groups.
- The exact age group of interest is not clear.  
- For current needs the relevant group may be children of school age.
- For future planning children aged 5 years and under may be more relevant.
- The user of the visualisation should be able to select *age*.

What do we know about existing schools?
========================================================

- In addition to Greek Language classes offered outside of school hours by the GOCMV there are also the following alternatives:
    - Public schools that offer Greek language as part of their curriculum,
    - Private Greek language schools that offer classes outside of school hours,
    - Parish based schools.
- The qualitative differences between these types of schools are important.
- The user of the visualisation should be able to select *provider*

The Shiny App
========================================================

- The visualisation should:
    - Have a layer that is a street map for context,
    - Allow the user to move around the map and zoom in and out,
    - Change depending on census question and age group,
    - Overlay the location of schools,
    - Provide useful information when the user clicks on the map.
- Using the `leaflet` and `shiny` packages satisfied these requirements.
- Let's have a look...

Data Sources
========================================================

- The TableBuilder facility provided by the Australian Bureau of Statistics (ABS) was used to collect data on the number of Greeks living in each region, broken down by age.
- Important data about the boundaries of each geographical region was also obtained from the ABS.
- Data on the schools including addresses were provided by the GOCMV with coordinates of their locations found using a tool found at <https://www.doogal.co.uk/BatchGeocoding.php>.

Preparing the Data
========================================================

- Much of the data processing does not need to be carried out each time the app is used.
- I wrote a small script to:
    - Combine the five csv files downloaded from TableBuilder.
    - Convert age into a numeric variable.
    - Convert these into a long format.
- All the pre-processing is documented in an R Markdown document that I am happy to share.

Started with this...
========================================================

![One csv File](csvFile.png)

... got to this.
========================================================


```
                SA2 Age question persons
1         Brunswick   0     ANC1       3
2    Brunswick East   0     ANC1       6
3    Brunswick West   0     ANC1       6
4            Coburg   0     ANC1      16
5 Pascoe Vale South   0     ANC1      14
```

Shapefiles
========================================================

- The regional unit used is called the *Statistical Area Level 2* or *SA2* from Australian Statistical Geography Standard (ASGS).
- The ABS provide shapefiles for the ASGS for download at 
<http://www.abs.gov.au/AUSSTATS/abs@.nsf/DetailsPage/1270.0.55.001July%202011?OpenDocument>.
- Shapefiles are a popular GIS format.
- They consist of multiple files in a single directory.
- They can be read into R using the `readOGR` function in the `rgdal` package.



```r
library(rgdal)
shapedata<-readOGR('../Preliminary/1270055001_sa2_2011_aust_shape/',"SA2_2011_AUST") 
```

Simplifying Shapefiles
========================================================

- For anything interactive it is crucial to simplify the shapefiles.
- Each SA2 is represented by a polygon, the shapefile stores the coordinates of the corners of the polygon.
- By removing some of these coordinates we get polygons that are more *low resolution* but still convey useful information.

```r
library(rmapshaper)
shapedatasimple<-ms_simplify(shapedata) 
object.size(shapedata)
```

```
83654816 bytes
```

```r
object.size(shapedatasimple)
```

```
12894536 bytes
```

Shiny - The User Interface
========================================================

- There are three *inputs*:
    - The age range (`sliderInput`) 
    - The census question (`selectInput`) 
    - The school provider (`checkboxGroupInput`) 
- There is a single *output*
    - The choropleth (`leafletOutput`)

Shiny - The Server side
========================================================

- Calculate/Update the school location data when: 
    - There is a change in the school provider.
- Calculate/Update the number of Greeks living in each SA2 when: 
    - There is a change in the census question,
    - There is a change in the age range.
- Draw the choropleth.
- Re-draw the layers of the plot, whenever inputs change.

Updating the Schools
========================================================


```r
   filteredSchools<-reactive({
     filter(Schools,Provider %in% input$prov)
   })
```

- The code above simply filters the Schools data by providers that have been ticked.
- Note the use of the `reactive` function ensures this code is only run when necessary.
- Here that is when `input$prov` changes.

Updating the number of Greeks in each SA2
========================================================


```r
filtereddata<-reactive({
     filter(Greeks,
            (question==input$question)&
              (Age>=input$Age[1])&
              (Age<=input$Age[2]))%>%
       group_by(.,SA2)%>%
       summarise(.,sum(persons))%>%
       rename(.,People=`sum(persons)`,
              SA2_NAME11=SA2)->out

     Shape<-sp::merge(Shape,out,
                      by="SA2_NAME11",all.x=TRUE)
   })
```

- Note here the use of `dplyr` functions and the long format.  
- Also the `merge` function from the `sp` package is used to attach population information to the shapefile data.

Drawing the street map layer
========================================================


```r
   output$lPlot<-renderLeaflet({
     leaflet() %>%
       addProviderTiles("Esri.WorldTopoMap")%>%
       setView(lng=144.9631,lat=-37.8136,zoom=10)
   })
```

- The code above draws the street map without any regions and centers it at Melbourne.  
- For the plot to be interactive use `renderLeaflet` rather than `leaflet`.


Adding the polygons
========================================================


```r
   leafletProxy("lPlot",data=filtereddata())%>%
       clearShapes()%>% 
       clearControls()%>% 
       addPolygons(fillColor = ~pal(People), 
               fillOpacity = 0.4, 
               color = "#BDBDC3", 
               weight = 2, 
               popup = sa2_popup, 
               highlightOptions=highlightOptions(color='black', weight=5)))
```
- This code is nested within the `observe` function and only redraws the regions when `filtereddata()` change.

The popup
========================================================

When clicking on a region it may be useful to know the exact name of the region and number of Greeks in that region.  We need to define a *popup*, for example:

```r
sa2_popup <- paste0("<strong>SA2: </strong>",
                           filtereddata()$SA2_NAME11, 
                           "<br><strong>People: </strong>", 
                           filtereddata()$People)
```

Bits of HTML
========================================================

- To do some things we require html (as on previous slide).  
- Another example of using html is to get labels on the checkboxes.  

```r
p("Community",
  span(icon('institution'),
       style=paste0("color:",cols[1])))
```
- The `icon` function gives us access to libraries of icons including the small building that looks like a Greek temple.

Lessons learnt - Building the App
========================================================

- Getting an interactive choropleth using `leaflet` and `shiny` is fairly simple and anyone competent in R can learn how to do it quite quickly.
- Having to redraw every layer of the map can be slow.  Divide the map up into layers and only redraw the layers that need to be changed.
- The functions `reactive`, `observe`, `leafletProxy` and `renderLeaflet` are useful but take a bit longer to master.
- Finally there can be a big effort in applying finishing touches (in my case - changing the orientation of the legend and drawing the little icons with colors).

Lessons learnt - Building a School
========================================================

- The GOCMV gave positive feedback about the app.
- On the basis of this work the GOCMV are considering sites for a school in the outer North and North West.
- Everything will have to be updated with 2016 census data. Tools for tracking workflow such as R markdown are crucial here.
- This project is a reminder of how visualisation can guide evidence based decision making.
- It is also a reminder of the value of census data.
    


